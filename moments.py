class ImageMoments:
    def __init__(self, image):
        if image is None:
            raise ValueError
        self.image = image
        self.image_height = image.shape[0] # first in loop
        self.image_width = image.shape[1]
        self.M1 = self.__get_M1(image)
        self.M2 = self.__get_M2(image)
        self.M3 = self.__get_M3(image)
        self.M4 = self.__get_M4(image)
        self.M5 = self.__get_M5(image)
        self.M6 = self.__get_M6(image)
        self.M7 = self.__get_M7(image)


    def __get_m(self, p, q, image):
        m_out = 0
        val = 0
        for i in range(self.image_height):
            for j in range(self.image_width):
                if image[i][j] == 255:# or image[i][j] == 1:
                    val = 1
                else:
                    val = 0
                m_out += pow(i,p) * pow(j,q)*val
        return m_out


    def __i_centr(self, image):
        return self.__get_m(1, 0, image) / self.__get_m(0, 0, image)

    def __j_centr(self, image):
        return self.__get_m(0, 1, image) / self.__get_m(0, 0, image)

    def __M_cen(self, p, q, image):
        M_out = 0
        icen = self.__i_centr(image)
        jcen = self.__j_centr(image)
        val = 0
        for i in range(self.image_height):
            for j in range(self.image_width):
                if image[i][j] == 255:# or image[i][j] == 1:
                    val = 1
                else:
                    val = 0
                M_out += pow(i - icen, p) * pow(j - jcen, q) * val
        return M_out

    def __get_M1(self, image):
        return (self.__M_cen(2, 0, image) + self.__M_cen(0, 2, image)) / pow(self.__get_m(0, 0, image), 2)

    def __get_M2(self, image):
        return (pow(self.__M_cen(2, 0, image) - self.__M_cen(0, 2, image), 2) + 4 * pow(self.__M_cen(1, 1, image), 2)) / pow(self.__get_m(0, 0, image), 4)

    def __get_M3(self, image):
        return (pow(self.__M_cen(3, 0, image) - 3 * self.__M_cen(1, 2, image), 2) + pow(3*self.__M_cen(2, 1, image) - self.__M_cen(0, 3, image), 2)  ) / pow(self.__get_m(0, 0, image), 5)

    def __get_M4(self, image):
        return (pow(self.__M_cen(3, 0, image) + self.__M_cen(1, 2, image), 2) + pow(self.__M_cen(2, 1, image) + self.__M_cen(0, 3, image), 2)  ) / pow(self.__get_m(0, 0, image), 5)

    def __get_M5(self, image):
        return None

    def __get_M6(self, image):
        return None

    def __get_M7(self, image):
        return ((self.__M_cen(2, 0, image) * self.__M_cen(0, 2, image)) - pow(self.__M_cen(1, 1, image), 2)) / pow(self.__get_m(0, 0, image), 4)
