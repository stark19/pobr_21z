import random
import numpy as np
import cv2

from moments import ImageMoments


class LogoFinder:
    def __init__(self, images_to_process, debug=False):
        self.images_to_process = images_to_process
        self.output_images = []
        self.debug = debug
        self.gray_treshold = 150 # useless in final algorithm
        self.blue_treshold_bounds = ((168,255),(85,123),(28,55)) # BGR format
        self.white_treshold_bounds = ((196,255),(201,255),(182,255)) # BGR format
        self.rect_color = (0, 0, 255)
        self.thickness = 2

    # Main function that process all the images
    def process_images(self, _image=None):
        if _image is None:
            for image in self.images_to_process:
                work_in_progess_image = image.copy()
                # grayscale_image = self._convert_to_grayscale(work_in_progess_image)
                gray_tresholded, blue_tresholded, white_tresholded = self._treshold_image_complex(work_in_progess_image,
                                                                                          blue_treshold_bounds=self.blue_treshold_bounds,
                                                                                          white_treshold_bounds=self.white_treshold_bounds)
                white = self.dilate_image(white_tresholded, dilation_level=5)
                white = self.dilate_image(white, dilation_level=5)

                blue = self.dilate_image(blue_tresholded, dilation_level=5)
                blue = self.dilate_image(blue, dilation_level=5)

                if self.debug:
                    cv2.imshow("Dilate white org", white_tresholded)
                    cv2.imshow("Dilate white", white)
                    cv2.imshow("Dilate blue", blue)
                    cv2.waitKey(0)

                white_bounding_boxes = self.flood_fill_iterative(white, image) # returns bounding boxes of objects
                blue_bounding_boxes = self.flood_fill_iterative(blue, image) # returns bounding boxes of objects

                output_image = self.find_and_mark_matching_countours(image, white_bounding_boxes, blue_bounding_boxes)
                cv2.imshow("Final product", output_image)
                cv2.waitKey(0)
                self.output_images.append(output_image)

        else:
            pass #procsess only one given image

    def find_and_mark_matching_countours(self, image, white_boundings, blue_boundings):
        image_copy = image.copy()
        image_grayscale = self._convert_to_grayscale(image_copy)
        image_treshold_white = self._treshold_image_grey(175, image_grayscale)
        for bound in white_boundings:
            width = bound[2]-bound[0]
            height = bound[3]-bound[1]
            if (height)*(width) > 500 and (1<=width/height<=1.6): # check if object is big enough
                roi = image_treshold_white[bound[1]:bound[3], bound[0]:bound[2]]
                try:
                    moments = ImageMoments(roi)
                except ZeroDivisionError:
                    break
                if self.check_moments(moments):
                    if self.debug:
                        cv2.imshow("ROI", roi)
                        cv2.waitKey(0)
                    cv2.rectangle(image_copy, (bound[0],bound[1]), (bound[2],bound[3]), self.rect_color, self.thickness)

        image_treshold_blue = self._treshold_image_grey(175, image_grayscale)
        # Invert colors before checking blue bounds
        image_treshold_blue[image_treshold_blue == 255] = np.uint8(100)
        image_treshold_blue[image_treshold_blue == 0] = np.uint8(255)
        image_treshold_blue[image_treshold_blue == 100] = np.uint8(0)
        for bound in blue_boundings:
            width = bound[2]-bound[0]
            height = bound[3]-bound[1]
            if (height)*(width) > 500 and (1<=width/height<=1.6): # check if object is big enough
                roi = image_treshold_blue[bound[1]:bound[3], bound[0]:bound[2]]
                try:
                    moments = ImageMoments(roi)
                except ZeroDivisionError:
                    break
                if self.check_moments(moments):
                    if self.debug:
                        cv2.imshow("ROI", roi)
                        cv2.waitKey(0)
                    cv2.rectangle(image_copy, (bound[0],bound[1]), (bound[2],bound[3]), self.rect_color, self.thickness)
        return image_copy


    def check_moments(self, moments):
        if (0.40 <= moments.M1 <= 0.66) and (0.015 <= moments.M2 <= 0.08) and \
                (0.0005 <= moments.M3 <= 0.004) and (0.0001 <= moments.M4 <= 0.0008) and (0.035 <= moments.M7 <= 0.08):
            return True
        return False

    def _convert_to_grayscale(self, image):
        image_height = image.shape[0]
        image_width = image.shape[1]
        output = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
        for x in range(image_height):
            for y in range(image_width):
                grayscale_pixel = (int(image[x][y][0]) + int(image[x][y][1]) + int(image[x][y][2])) / 3
                output.itemset((x,y), np.uint8(grayscale_pixel))
        return output

    def _treshold_image_complex(self, color_image, treshold=150, blue_treshold_bounds=None, white_treshold_bounds=None, gray_image=None):
        image_height = color_image.shape[0]
        image_width = color_image.shape[1]
        output_gray = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
        output_white = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
        output_blue = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
        for x in range(image_height):
            for y in range(image_width):
                if gray_image is not None:
                    output_gray[x][y] = self.__gray_check(gray_image[x][y], treshold)
                output_white[x][y] = self.__white__check(color_image[x][y], white_treshold_bounds)
                output_blue[x][y] = self.__blue__check(color_image[x][y], blue_treshold_bounds)
        if self.debug:
            if gray_image is not None:
                cv2.imshow("Treshold gray", output_gray)
            cv2.imshow("Treshold white", output_white)
            cv2.imshow("Treshold blue", output_blue)
            cv2.waitKey(0)
        return output_gray, output_blue, output_white

    def _treshold_image_grey(self, treshold=150, gray_image=None):
        image_height = gray_image.shape[0]
        image_width = gray_image.shape[1]
        output_gray = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
        for x in range(image_height):
            for y in range(image_width):
                if gray_image is not None:
                    output_gray[x][y] = self.__gray_check(gray_image[x][y], treshold)
        return output_gray

    def __gray_check(self, pixel, treshold):
        if pixel  > treshold:
            return 255
        else:
            return 0

    def __white__check(self, pixel, white_values):
        if pixel[0] >= white_values[0][0] and pixel[0] <= white_values[0][1] and \
                pixel[1] >= white_values[1][0] and pixel[1] <= white_values[1][1] and \
                pixel[2] >= white_values[2][0] and pixel[2] <= white_values[2][1]:
            return 255
        else:
            return 0

    def __blue__check(self, pixel, blue_values):
        if pixel[0] >= blue_values[0][0] and pixel[0] <= blue_values[0][1] and \
                pixel[1] >= blue_values[1][0] and pixel[1] <= blue_values[1][1] and \
                pixel[2] >= blue_values[2][0] and pixel[2] <= blue_values[2][1]:
            return 255
        else:
            return 0

    def dilate_image(self, image_src, dilation_level=3):
        # setting the dilation_level
        dilation_level = 3 if dilation_level < 3 else dilation_level

        # obtain the kernel by the shape of (dilation_level, dilation_level)
        structuring_kernel = np.full(shape=(dilation_level, dilation_level), fill_value=255)

        orig_shape = image_src.shape
        pad_width = dilation_level - 2

        # pad the image with pad_width
        image_pad = np.pad(array=image_src, pad_width=pad_width, mode='constant')
        pimg_shape = image_pad.shape
        h_reduce, w_reduce = (pimg_shape[0] - orig_shape[0]), (pimg_shape[1] - orig_shape[1])

        # obtain the submatrices according to the size of the kernel
        flat_submatrices = np.array([
            image_pad[i:(i + dilation_level), j:(j + dilation_level)]
            for i in range(pimg_shape[0] - h_reduce) for j in range(pimg_shape[1] - w_reduce)
        ])

        # replace the values either 255 or 0 by dilation condition
        image_dilate = np.array([np.uint8(255) if (i == structuring_kernel).any() else np.uint8(0) for i in flat_submatrices])
        # obtain new matrix whose shape is equal to the original image size
        image_dilate = image_dilate.reshape(orig_shape)

        return image_dilate

    def erode_image(self, image_src, erosion_level=3):
        erosion_level = 3 if erosion_level < 3 else erosion_level
        img_src_copy = image_src.copy()
        structuring_kernel = np.full(shape=(erosion_level, erosion_level), fill_value=255)

        orig_shape = image_src.shape
        pad_width = erosion_level - 2

        # pad the matrix with `pad_width`
        image_pad = np.pad(array=image_src, pad_width=pad_width, mode='constant')
        pimg_shape = image_pad.shape
        h_reduce, w_reduce = (pimg_shape[0] - orig_shape[0]), (pimg_shape[1] - orig_shape[1])

        # sub matrices of kernel size
        flat_submatrices = np.array([
            image_pad[i:(i + erosion_level), j:(j + erosion_level)]
            for i in range(pimg_shape[0] - h_reduce) for j in range(pimg_shape[1] - w_reduce)
        ])

        # condition to replace the values - if the kernel equal to submatrix then 255 else 0
        image_erode = np.array([np.uint8(255) if (i == structuring_kernel).all() else np.uint8(0) for i in flat_submatrices])
        image_erode = image_erode.reshape(orig_shape)

        return image_erode

    def flood_fill_recursive(self, image):
        width = image.shape[1]
        height = image.shape[0]

        def fill(x, y, start_color, color_to_update):
            if image[x][y] != start_color:
                return
            elif image[x][y] == color_to_update:
                return
            else:

                image[x][y] = color_to_update
                neighbors = [(x - 1, y), (x + 1, y), (x - 1, y - 1), (x + 1, y + 1), (x - 1, y + 1), (x + 1, y - 1),
                             (x, y - 1), (x, y + 1)]
                for n in neighbors:
                    if 0 <= n[0] <= width - 1 and 0 <= n[1] <= height - 1:
                        fill(n[0], n[1], start_color, color_to_update)

        # random starting point
        value = 0
        start_x = 0
        start_y = 0
        while value == 0:
            start_x = random.randint(0, width - 1)
            start_y = random.randint(0, height - 1)
            value = image[start_x][start_y]
        start_color = image[start_x][start_y]
        fill(start_x, start_y, start_color, 100)
        return image

    def is_white(self, grid, row, col):

        if (row < 0 or row > len(grid) - 1):
            return False

        if (col < 0 or col > len(grid[0]) - 1):
            return False

        if grid[row][col] == 255:
            return True
        else:
            return False

    def flood_fill_iterative(self, image, color_image):
        found_objects = []
        grid = image.copy()
        color_image_copy = color_image.copy()
        image_height = image.shape[0]
        image_width = image.shape[1]
        for x in range(image_height):
            for y in range(image_width):
                found_obj = self._flood_fill_iterative_alg(grid, x, y)
                if found_obj is not None:
                    found_objects.append(found_obj)
        rect_color = (0, 0, 255)
        thickness = 2
        if self.debug:
            for img in found_objects:
                cv2.rectangle(color_image_copy, (img[0],img[1]), (img[2],img[3]), rect_color, thickness)
            cv2.imshow("Flood Fill rectangles", color_image_copy)
            cv2.waitKey(0)
        return found_objects

    def _flood_fill_iterative_alg(self, grid, row, col):

        if (row < 0 or row > grid.shape[0] - 1):
            return None

        if (col < 0 or col > grid.shape[1] - 1):
            return None

        if (grid[row][col] != 255):
            return None

        min_x = 9999
        min_y= 9999
        max_x = 0
        max_y = 0
        q = []  # FIFO que
        grid[row][col] = 100  # mark as visited
        q.append([row, col])  # add to queue

        while len(q) > 0:
            [cur_row, cur_col] = q[0]
            del q[0]
            if cur_row < min_x:
                min_x = cur_row
            if cur_col < min_y:
                min_y = cur_col
            if cur_row > max_x:
                max_x = cur_row
            if cur_col > max_y:
                max_y = cur_col
            if (self.is_white(grid, cur_row - 1, cur_col) == True):
                grid[cur_row - 1][cur_col] = np.uint8(100)
                q.append([cur_row - 1, cur_col])

            if (self.is_white(grid, cur_row + 1, cur_col) == True):
                grid[cur_row + 1][cur_col] = np.uint8(100)
                q.append([cur_row + 1, cur_col])

            if (self.is_white(grid, cur_row, cur_col - 1) == True):
                grid[cur_row][cur_col - 1] = np.uint8(100)
                q.append([cur_row, cur_col - 1])

            if (self.is_white(grid, cur_row, cur_col + 1) == True):
                grid[cur_row][cur_col + 1] = np.uint8(100)
                q.append([cur_row, cur_col + 1])
        return min_y, min_x, max_y, max_x

    def show_original_images(self):
        for index, image in enumerate(self.images_to_process):
            cv2.imshow(index, image)

    def show_processed_images(self, image=None, save=False):
        if image is None:
            for index, image in enumerate(self.output_images):
                cv2.imshow("P"+str(index+1), image)
                if save:
                    cv2.imwrite("P" + str(index + 1)+"_processed.jpg", image)
        else:
            cv2.imshow("Given image", image)
        cv2.waitKey(0)