import cv2
import os
from moments import ImageMoments
from help_tools import tracking_function, treshold_image_grey, convert_to_grayscale
from logo_finder import LogoFinder

mode = "normal"
# "gui"
# "normal"
# "template"

class FileLoader:
    def __init__(self, path_to_folder="data"):
        self.images_list = []
        for filename in os.listdir(path_to_folder):
            img = cv2.imread(os.path.join(path_to_folder, filename))
            if img is not None:
                self.images_list.append(img)

    def save_images_to_folder_with_tag(self, images, path_to_folder='output'):
        for index, image in enumerate(images):
            img = cv2.imwrite(os.path.join(path_to_folder, "p" + str(index) + "_output.jpg"))
            if img is not None:
                self.images_list.append(img)


if __name__ == '__main__':
    if mode == "gui":
        tracking_function('data/p7.jpg', mode="BGR")
    elif mode == "template":
        my_moments = []
        for filename in os.listdir("template"):

            img = cv2.imread(os.path.join("template", filename))
            if img is not None:
                img = convert_to_grayscale(img)
                img = treshold_image_grey(175, img)
                if filename == "p8.jpg":
                    img[img==255]=100
                    img[img==0]=255
                    img[img==100]=0
                    cv2.imwrite(filename+"_t.png", img)
                else:
                    cv2.imwrite(filename+"_t.png", img)

            my_moments.append(ImageMoments(img))
    elif mode == "normal":
        file_loader = FileLoader("data")
        my_logo_finder = LogoFinder(file_loader.images_list, debug=True)
        my_logo_finder.process_images()
        my_logo_finder.show_processed_images(save=True)
        cv2.waitKey(0)