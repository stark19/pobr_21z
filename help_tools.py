import numpy as np
import cv2

def tracking_function(img_path, mode):
    def nothing(x):
        pass
    if mode == "HSV":
        cv2.namedWindow("Tracking")
        cv2.createTrackbar("LH", "Tracking", 0, 255, nothing)
        cv2.createTrackbar("LS", "Tracking", 0, 255, nothing)
        cv2.createTrackbar("LV", "Tracking", 0, 255, nothing)
        cv2.createTrackbar("UH", "Tracking", 255, 255, nothing)
        cv2.createTrackbar("US", "Tracking", 255, 255, nothing)
        cv2.createTrackbar("UV", "Tracking", 255, 255, nothing)
    elif mode == "BGR":
        cv2.namedWindow("Tracking")
        cv2.createTrackbar("LB", "Tracking", 0, 255, nothing)
        cv2.createTrackbar("LG", "Tracking", 0, 255, nothing)
        cv2.createTrackbar("LR", "Tracking", 0, 255, nothing)
        cv2.createTrackbar("UB", "Tracking", 255, 255, nothing)
        cv2.createTrackbar("UG", "Tracking", 255, 255, nothing)
        cv2.createTrackbar("UR", "Tracking", 255, 255, nothing)

    frame = cv2.imread(img_path)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    while True:

        if mode == "HSV":
            l_h = cv2.getTrackbarPos("LH", "Tracking")
            l_s = cv2.getTrackbarPos("LS", "Tracking")
            l_v = cv2.getTrackbarPos("LV", "Tracking")

            u_h = cv2.getTrackbarPos("UH", "Tracking")
            u_s = cv2.getTrackbarPos("US", "Tracking")
            u_v = cv2.getTrackbarPos("UV", "Tracking")

            l_b = np.array([l_h, l_s, l_v])
            u_b = np.array([u_h, u_s, u_v])

            mask = cv2.inRange(hsv, l_b, u_b)
            res = cv2.bitwise_and(frame, frame, mask=mask)

            cv2.imshow("frame", frame)
            cv2.imshow("mask", mask)
            cv2.imshow("res", res)

        elif mode == "BGR":
            l_b = cv2.getTrackbarPos("LB", "Tracking")
            l_g = cv2.getTrackbarPos("LG", "Tracking")
            l_r = cv2.getTrackbarPos("LR", "Tracking")

            u_b = cv2.getTrackbarPos("UB", "Tracking")
            u_g = cv2.getTrackbarPos("UG", "Tracking")
            u_r = cv2.getTrackbarPos("UR", "Tracking")

            l_b = np.array([l_b, l_g, l_r])
            u_b = np.array([u_b, u_g, u_r])

            mask = cv2.inRange(frame, l_b, u_b)

            res = cv2.bitwise_and(frame, frame, mask=mask)

            cv2.imshow("frame", frame)
            cv2.imshow("mask", mask)
            cv2.imshow("res", res)

        key = cv2.waitKey(1)
        if key == 27:
            break
    cv2.destroyAllWindows()

def treshold_image_grey(treshold=150, gray_image=None):
    image_height = gray_image.shape[0]
    image_width = gray_image.shape[1]
    output_gray = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
    for x in range(image_height):
        for y in range(image_width):
            if gray_image is not None:
                output_gray[x][y] = __gray_check(gray_image[x][y], treshold)
    return output_gray

def __gray_check(pixel, treshold):
    if pixel  > treshold:
        return 255
    else:
        return 0


def convert_to_grayscale(image):
    image_height = image.shape[0]
    image_width = image.shape[1]
    output = np.zeros((image_height, image_width), dtype=np.dtype('uint8'))
    for x in range(image_height):
        for y in range(image_width):
            grayscale_pixel = (int(image[x][y][0]) + int(image[x][y][1]) + int(image[x][y][2])) / 3
            output.itemset((x,y), np.uint8(grayscale_pixel))
    return output